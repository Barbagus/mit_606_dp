def fibonacci(n: int):
    """Return the n-th Fibonacci number."""

    memo: dict[int, int] = {}
    """The memoization register."""

    def dp(i: int):
        """Recursive subproblem: return memoized i-th Fibonacci number."""

        # check if memoized yet
        f = memo.get(i, None)
        if f is not None:
            return f

        # compute actual value, making "free" recursive calls
        f = 2 if i <= 2 else dp(i-1) + dp(i-2)

        # set the memoization register
        memo[i] = f

        return f

    return dp(n)


print(f"The 9th Fibonacci number is: {fibonacci(9)}")
