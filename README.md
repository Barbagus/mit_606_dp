# MIT 6.006 Introduction to Algorithms

Implementations of some dynamic programing examples outlined in the available material on youtube.

- 2011
  - [Dynamic Programming I: Fibonacci, Shortest Paths](https://www.youtube.com/watch?v=OQ5jsbhAv_M)
  - [Dynamic Programming II: Text Justification, Blackjack](https://www.youtube.com/watch?v=ENyox7kNKeY)
  - [Dynamic Programming III: Parenthesization, Edit Distance, Knapsack](https://www.youtube.com/watch?v=ocZMDMZwhCY)
  - [Dynamic Programming IV: Guitar Fingering, Tetris, Super Mario Bros.](https://www.youtube.com/watch?v=tp4_UXaVyx8)

- 2020
  - [Part 1: SRTBOT, Fib, DAGs, Bowling](https://www.youtube.com/watch?v=r4-cftqTcdI)
  - [Part 2: LCS, LIS, Coins](https://www.youtube.com/watch?v=KLBCUx1is2c)
  - [Part 3: APSP, Parens, Piano](https://www.youtube.com/watch?v=TDo3r5M1LNo)
  - [Part 4: Rods, Subset Sum, Pseudopolynomial](https://www.youtube.com/watch?v=i9OAOk0CUQE)
